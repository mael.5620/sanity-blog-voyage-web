const sanityClient = require('@sanity/client')

module.exports = sanityClient({
    projectId: 'ylqu1g51',
    dataset: 'production',
    useCdn: true
})