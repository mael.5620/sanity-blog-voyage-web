import React from 'react';
import imageUrlBuilder from '@sanity/image-url'
import Link from 'next/link'
import client from '../../client'
import styles from './PostCard.module.css'

// TODO: Mise en forme des cartes preview des 4 derniers posts 

const urlFor = source => (
    imageUrlBuilder(client).image(source)
)

const PostCard = ({content}) => {
    return (
        <div>
            <Link href={`/post/${content.slug.current}`} >
                <p>{content.title}</p>
            </Link>
            <img src={urlFor(content.poster)} alt="" width={300}/>
            <p>{content.description}</p>
        </div>
    );
};

export default PostCard;