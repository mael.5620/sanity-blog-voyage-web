import Header from '../Header/Header';
import styles from './Hero.module.css'

const Hero = () => {
    return (
        <div className={styles.container} >
            <Header />
            <div className={styles.text_container} >
                <p className={styles.text} >Le blog du <br/> <strong>Voyageur</strong></p>

            </div>
        </div>
    );
};

export default Hero;