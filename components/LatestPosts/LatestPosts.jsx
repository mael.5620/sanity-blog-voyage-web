import PostCard from '../PostCard/PostCard'
import styles from './LatestPost.module.css'

const LatestPosts = ({posts}) => {
    console.log(posts)
    return (
        <div className={styles.container}>
          {
              posts.map(post => (
                  <PostCard content={post} />
              ))
          }  
        </div>
    );
};

export default LatestPosts;