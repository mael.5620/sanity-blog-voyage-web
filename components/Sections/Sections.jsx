import classnames from 'classnames'
import LatestPosts from '../Latestposts/LatestPosts'
import styles from './Sections.module.css'

const Sections = ({posts}) => {
    return (
        <div className={styles.sections_container} >
            <section className={styles.section} >
                <h2 className={styles.title} >Bienvenue</h2>
                <p className={styles.text} >Bonjour et bienvenue sur ce blog de récit de voyages.  Ici vous trouverez de nombreux récits de voyages de nos nombreux membres. En outre, dans la partie "Guides", vous retrouverez des guides mit en vente par les membres. <br/> <br/> Ces guides vous aiderons par exemple a aménager vous même votre fourgon ou a découvrir de nombreuses astuces sur le voyage en vélo. </p>
            </section>
            <section className={classnames(styles.section, styles.latest_posts)} >
                <h2 className={styles.latest_posts_title} >Dernières publications</h2>
                <p>Découvrez les publications récentes faites par nos membres</p>
                <LatestPosts posts={posts} />
            </section>
            <section className={styles.section} >
                <h2 className={styles.title}>Publications par catégories</h2>
                <p>Vous êtes intérésser par un thème en particulier ? <br/> Trouvez les articles qui en parlent</p>
                {/* TODO: Composant: Catégories d'articles + voir tout */}
            </section>
        </div>
    );
};

export default Sections;