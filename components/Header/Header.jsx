import { useRouter } from 'next/router'
import styles from './Header.module.css'

const Header = () => {
    const { pathname } = useRouter()
    
    return (
        <div className={styles.container} >
            <a className={pathname == "/" ? styles.link_active : styles.link} href="/">Accueil</a>
            <a className={pathname == "/publications" ? styles.link_active : styles.link} href="/publications">Publications</a>
            <a className={pathname == "/guides" ? styles.link_active : styles.link} href="/guides">Guides</a>
            <a className={pathname == "/contact" ? styles.link_active : styles.link} href="/contact">Contact</a>
        </div>
    );
};

export default Header;