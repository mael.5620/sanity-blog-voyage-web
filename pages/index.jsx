import groq from 'groq'
import client from '../client'
import styles from '../styles/home.module.css'
import Hero from '../components/Hero/Hero'
import Sections from '../components/Sections/Sections'
import Footer from '../components/Footer/Footer'

const index = ({posts}) => {
  console.log(posts)
  return (
    <div>
      <Hero />
      <Sections posts={posts} />
      <Footer />
    </div>
  );
};

// Get latests posts
index.getInitialProps = async () => ({
  posts: await client.fetch(groq`*[_type == "post"] | order(_createdAt desc) | [0...4]`)
})
export default index;
