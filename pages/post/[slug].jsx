import React from 'react';
import groq from 'groq'
import client from '../../client';

// TODO: Affichage du post en entier 

const post = ({ post }) => {
    return (
        <div>
            {post.title}
        </div>
    );
};


export async function getStaticPaths() {
    const query = groq`*[_type == "post"]`
    const posts = await client.fetch(query)
    const paths = posts.map(post => ({
        params: { slug: post.slug.current }
    }))
    return { paths, fallback: true }
}

export async function getStaticProps({params}) {
    
    const { slug = "" } = params
    const query = groq`*[_type == "post" && slug.current == "${slug}"][0]`
    const post = await client.fetch(query)
    return { props: { post } }
}

export default post;