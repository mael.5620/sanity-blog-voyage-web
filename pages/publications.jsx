import groq from 'groq';
import client from '../client'
import Header from '../components/Header/Header';

// TODO: Lister tous les posts 
// FEATURE: Trier les publications par theme
// FEATURE: Fonctionnalitée de recherche dans les publications

const publications = ({posts}) => {
    return (
        <div>
            <Header />
            Liste des publications
        </div>
    );
};

export async function getStaticProps() {
    const query = groq`*[_type == "post"] | order(_createdAt desc)`
    const posts = await client.fetch(query)
    return { props: { posts } }
}

export default publications;